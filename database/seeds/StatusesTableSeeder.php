<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$status = new Status();
		$status->title = 'TODO';
		$status->description = 'Сделать';
		$status->save();

		$status = new Status();
		$status->title = 'DOING';
		$status->description = 'Делается';
		$status->save();

		$status = new Status();
		$status->title = 'DONE';
		$status->description = 'Сделано';
		$status->save();
    }
}

<?php

return [
    'welcome' => 'Welcome to our application',
    'task.delete' => 'Task Deleted',
    'task.update' => 'Task Updated',
    'task.store' => 'Task saved successfully',
    'user.delete' => 'User deleted successfully',
    'user.update' => 'User updated successfully',
    'user.store' => 'User saved successfully',
];
@extends('layouts.app')

@section('content')
<div class="shadow-sm p-3 mb-5 bg-white rounded">
    <div class="col-md-8">
        <div class="jumbotron shadow p-3 mb-5 rounded border border-dark">

            <h1 class="display-4">Task List</h1>
            <p class="lead">Тестовое задание</p>

            <hr class="my-4">

            <div>
                <a href="{{ route('api.tasks') }}">API</a>
                <hr class="my-4">
                <a class="btn btn-primary btn-lg" href="{{ route('tasks.index') }}" role="button">Перейти к списку</a>
            </div>
        </div>
    </div>
</div>


@endsection
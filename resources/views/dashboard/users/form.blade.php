    @csrf
    <div class="form-group col-md-8">

        {{ Form::label('InputUserName', 'Имя') }}

        {{ Form::text('name', $user->name ?? old('name'),
            array_merge([
            'class' => $errors->has('name') ? 'form-control is-invalid' : 'form-control'
            ],
            $attributes = [
                'id' => 'InputUserName',
                'aria-describedby' => 'InputUserNameHelp',
                'placeholder' => 'Имя'
            ])) }}

        @if ($errors->has('name'))
            <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif

        <small id="InputUserNameHelp" class="form-text text-muted">Имя пользователя</small>

    </div>

    <div class="form-group col-md-8">

        {{ Form::label('InputUserEmail', 'Email') }}

        {{ Form::text('email', $user->email ?? old('email'),
            array_merge([
            'class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control'
            ],
            $attributes = [
                'id' => 'InputUserEmail',
                'aria-describedby' => 'InputUserEmailHelp',
                'placeholder' => 'E-Mail'
            ])) }}

        @if ($errors->has('email'))
            <span class="invalid-feedback">
            <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <small id="InputUserEmailHelp" class="form-text text-muted">Почта пользователя</small>

    </div>

    

    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('users.index') }}" type="button" class="btn btn-primary">Отмена</a>

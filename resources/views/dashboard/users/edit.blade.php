@extends('dashboard.layouts.app')

@section('content')

    <form method="post" action="{{ route('users.update', $user->id) }}">
    {{ method_field('PUT') }}
        @include('dashboard.users.form')

    </form>

@endsection
@extends('dashboard.layouts.app')

@section('content')

    <form method="post" action="{{ route('users.store') }}">

        @include('dashboard.users.form')

    </form>

@endsection
@extends('dashboard.layouts.app')



@section('content')

    <main class="container pt-5">
        <div class="card mb-5 box-shadow">
            <div class="card-header">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">{{ $title or 'Нет заголовка' }}</h3>

                        </div>
                        <div class="col col-xs-6 text-right">
                            <a class="btn btn-sm btn-primary btn-create" href="{{ route('users.create') }}" role="button">Создать пользователя</a>
                        </div>
                    </div>


            </div>
            <div class="card-block p-0 table-responsive">
                <table class="table table-hover table-sm">
                    <thead class="">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Потверждние почты</th>
                           
                            <th><em class="fa fa-cog"></em></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                       @forelse ($users as $user)
                        <tr>
                            <td class="hidden-xs">{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->confirmed ? 'Да' : 'Нет' }}</td>
                            
                            <td align="center">
                                <a class="btn btn-outline-success" href="{{ route('users.edit', $user->id) }}" title="Редактировать"><i class="fas fa-edit"></i></a>
                            </td>
                            <td>
                                

                                {!! Form::open(['method' => 'DELETE','route' => [
                                    'users.destroy', $user->id],
                                    'style'=>'display:inline'
                                    ]) !!}
                                {!! Form::button('<i class="fa fa-trash fa-lg"></i>', [
                                    'class' => 'btn btn-danger',
                                    'data-toggle' => 'confirmation',
                                    'data-title' => 'Вы уверены?',
                                    'data-btn-ok-label' => 'Да',
                                    'data-btn-cancel-label' => 'Нет',
                                    'data-popout' => 'true',
                                    'data-singleton' => 'true',
                                    ]) !!}
                                {!! Form::close() !!}

                            </td>
                        
                        </tr>
                        @empty
                            <td class="table-warning" colspan="7"  style="text-align:center">Empty</td>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="card-footer p-3">
                {{ $users->links() }}
            </div>
        </div>
        
    </main>


@endsection
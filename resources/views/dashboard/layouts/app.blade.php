@include('dashboard.includes.head')

  <body>

@include('dashboard.widgets.navbar')

    <div id="app" class="container-fluid">
      <div class="row">
        
        @include('dashboard.widgets.sidebar')

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            {{ Breadcrumbs::render() }}
          </div>
          
          @if (session('status'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                  {{ session('status') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>

          @endif
          
          <h2>{{ $title or 'Нет заголовка' }}</h2>

          @yield('content')

        </main>
      </div>
    </div>

@include('dashboard.includes.footer')
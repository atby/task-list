@extends('dashboard.layouts.app')

@section('content')

    <form method="post" action="{{ route('tasks.store') }}">

        @include('dashboard.tasks.form')

    </form>

@endsection
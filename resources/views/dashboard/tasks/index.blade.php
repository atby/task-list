@extends('dashboard.layouts.app')

@section('content')

<a class="btn btn-success btn-lg btn-block" href="{{ route('tasks.create') }}" role="button">Создать задачу</a>
<br>
<div class="row">
    @forelse ($statuses as $status)
        <div class="col-sm-4">
            <div class="card">
              <div class="card-header bg-secondary">
                {{ $status->title }}
              </div>
              @forelse ($tasks as $task)
                <ul class="list-group list-group-flush">
                  @if ($status->id === $task->status_id)
                  <li class="list-group-item">
                      <h5 class="card-title">{{ $task->title }}</h5>
                      <p class="card-text">{{ $task->description }}</p>
                      <p class="card-text">Комментариев: {{ $task->comments_count }}</p>
                      <p class="card-text"><small class="text-muted">Создано: {{ $task->created_at ? $task->created_at->diffForHumans() : 'Нет даты добавления' }}</small></p>
                      <p class="card-text"><small class="text-muted">Обновлено: {{ $task->updated_at ? $task->updated_at->diffForHumans() : 'Нет даты обновления' }}</small></p>
                      <a class="btn btn-outline-success btn-block" href="{{ route('tasks.edit', $task->id) }}" title="Редактировать"><i class="fas fa-edit"></i></a>
                      <form action="{{ route('tasks.destroy', $task->id) }}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('delete') }}
                              <button class="btn btn-outline-dark btn-block" onclick="return confirm('Хотите удалить?')" type="submit" title="Удалить"><i class="fas fa-trash-alt"></i></button>
                      </form>
                  </li>
                  @endif
                </ul>
              @empty
                <div class="alert alert-info" role="alert">
                  <h5 class="alert-heading">Нет записей</h5>
                </div>
              @endforelse
            </div>
        </div>
    @empty
      <div class="alert alert-info" role="alert">
        <h5 class="alert-heading">Нет записей</h5>
      </div>
    @endforelse
</div>

@endsection
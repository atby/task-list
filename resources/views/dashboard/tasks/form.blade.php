    @csrf
    <div class="form-group col-md-8">
        
        {{ Form::label('InputTaskTitle', 'Заголовок') }}

        {{ Form::text('title', $task->title ?? old('title'),
            array_merge([
            'class' => $errors->has('title') ? 'form-control is-invalid' : 'form-control'
            ],
            $attributes = [
                'id' => 'InputTaskTitle',
                'aria-describedby' => 'InputTaskTitleHelp',
                'placeholder' => 'Заголовок'
            ])) }}

        @if ($errors->has('title'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif

        <small id="InputTaskTitleHelp" class="form-text text-muted">Заголовок задачи</small>
    </div>

    <div class="form-group col-md-8">

        {{ Form::label('InputTaskDescription', 'Описание задачи') }}

        {{ Form::textarea('description', $task->description ?? old('description'),
            array_merge([
                'class' => $errors->has('description') ? 'form-control is-invalid' : 'form-control'
            ],
            $attributes = [
                'id' => 'InputTaskDescription',
                'aria-describedby' => 'InputTaskDescriptionHelp',
                'placeholder' => 'Описание'
            ])) }}

        @if ($errors->has('description'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif

        <small id="InputTaskDescriptionHelp" class="form-text text-muted">Короткое описание задачи</small>

    </div>

    <div class="form-group col-md-4">

        {{ Form::label('InputStatus', 'Статус задачи') }}

        @if (count($statuses) !== 0)
            {!! Form::select('status_id', 
                $statuses->pluck('title','id'),
                $task->status->id ?? null,
                [
                    'class' => 'form-control',
                    'id' => 'InputStatus'
                    ]) !!}
        @else
            <div class="alert alert-info" role="alert">
                <h5 class="alert-heading">Необходимо создать статус.</h5>
            </div>
        @endif

    </div>
    
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{ route('tasks.index') }}" type="button" class="btn btn-primary">Отмена</a>



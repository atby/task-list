@extends('dashboard.layouts.app')

@section('content')

    <form method="post" action="{{ route('tasks.update', $task->id) }}">
    {{ method_field('PUT') }}
		@include('dashboard.tasks.form')
	</form>


@include('dashboard.comments.form')

@endsection
<h5 class="border-bottom border-gray pb-2 mb-0">Комментарии</h5>

    <div class="my-3 p-3 bg-white rounded box-shadow">
        <div class="form-group row add">
            <div class="col-md-8">
                <input type="text" class="form-control" id="name" name="content" placeholder="Комментарий" required>
                <input id="task" name="task_id" type="hidden" value="{{ $task->id }}">
                <p class="error text-center alert alert-danger invisible"></p>
            </div>
            <div class="col-md-4">
                <button class="btn btn-primary" type="submit" id="add">
                    <span class="fas fa-plus"></span> Добавить
                </button>
            </div>
        </div>
           {{ csrf_field() }}
        <div class="table-responsive text-center">
            <table class="table table-striped" id="table">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th class="text-center">Комментарий</th>
                        <th class="text-center">Добавлен</th>
                        <th class="text-center">Обновлен</th>
                        <th class="text-center">Действия</th>
                    </tr>
                </thead>
                @forelse ($task->comments as $comment)
                    <tr class="item{{ $comment->id }}">
                        <td></td>
                        <td>{{ $comment->content }}</td>
                        <td>{{ $comment->created_at }}</td>
                        <td>{{ $comment->updated_at }}</td>
                        <td><button class="edit-modal btn btn-info" data-id="{{ $comment->id }}"
                                data-name="{{ $comment->content }}" title="Редактировать">
                                <span class="fas fa-edit"></span>
                            </button>
                            <button class="delete-modal btn btn-danger"
                                data-id="{{ $comment->id }}" data-name="{{ $comment->content }}" title="Удалить">
                                <span class="fas fa-trash-alt"></span> 
                            </button></td>
                    </tr>
                @empty
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      Пока нет комментариев
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                @endforelse
            </table>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="fid">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Комментарии</label>
                            <div class="col-sm-10">
                                <input type="name" class="form-control" id="n">
                            </div>
                        </div>
                    </form>
                    <div class="deleteContent">
                        <input type="hidden" class="form-control" id="did">Удалить комментарий: <span class="dname"></span> ? 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn actionBtn" data-dismiss="modal">
                            <span id="footer_action_button"></span>
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='fas fa-times-circle'></span> Закрыть
                        </button>
                    </div>
                </div>
            </div>
          </div>
    </div>



<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="{{ route('dashboard.index') }}">
          <span data-feather="home"></span>
          Dashboard <span class="sr-only">(current)</span>
        </a>
      </li>
    </ul>
  <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
    <span>Администрирование</span>
  </h6>
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('users.index') }}">
          <span data-feather="file"></span>
          Пользователи
        </a>
      </li>
    </ul> 
  <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
    <span>Список задач</span>
  </h6>
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('tasks.index') }}">
          <span data-feather="file"></span>
          Задачи
        </a>
      </li>           
    </ul>
  </div>
</nav>
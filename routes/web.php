<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| 
| 
| 
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
|
|
|
|
|
*/

Auth::routes();

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| 
| 
| 
|
*/


Route::group([
    'prefix' => 'dashboard',
    ], function()
    {
        Route::get('/','Dashboard\DashboardController@index')->name('dashboard.index');
        Route::resource('/tasks', 'Dashboard\TaskController');
        Route::resource('/comments', 'Dashboard\CommentController');
        Route::resource('/users', 'Dashboard\UserController');
    });
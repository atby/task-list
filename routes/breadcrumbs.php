<?php

// Home
Breadcrumbs::for('index', function ($trail) {
    $trail->push('Home', route('index'));
});

// Dashboard
Breadcrumbs::for('dashboard.index', function ($trail) {
    $trail->push('Dashboard', route('dashboard.index'));
});

// Dashboard > Tasks
Breadcrumbs::for('tasks.index', function ($trail) {
	$trail->parent('dashboard.index');
	$trail->push('Задачи', route('tasks.index'));
});

Breadcrumbs::for('tasks.create', function ($trail) {
	$trail->parent('tasks.index');
	$trail->push('Создание задачи', route('tasks.create'));
});

Breadcrumbs::for('tasks.edit', function ($trail, $task){
	$trail->parent('tasks.index');
	$trail->push($task->title, route('tasks.edit', $task->id));
});

// Dashboard > Users
Breadcrumbs::for('users.index', function ($trail) {
	$trail->parent('dashboard.index');
	$trail->push('Пользователи', route('users.index'));
});

Breadcrumbs::for('users.create', function ($trail) {
	$trail->parent('users.index');
	$trail->push('Создание пользователя', route('users.create'));
});

Breadcrumbs::for('users.edit', function ($trail, $user){
	$trail->parent('users.index');
	$trail->push($user->name, route('users.edit', $user->id));
});

/*// Home > About
Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push('About', route('about'));
});

// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::for('category', function ($trail, $category) {
    $trail->parent('blog');
    $trail->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});*/
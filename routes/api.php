<?php

use Illuminate\Http\Request;
use App\Task;
use App\Http\Resources\Task as TaskResource;
use App\Http\Resources\TaskCollection;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/tasks', function() {
    return new TaskCollection(Task::all());
})->name('api.tasks');


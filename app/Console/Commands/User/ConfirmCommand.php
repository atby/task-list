<?php

namespace App\Console\Commands\User;

use Illuminate\Console\Command;
use App\Services\Auth\RegisterService;
use App\User;

class ConfirmCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:confirm {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Confirm user registration';

    private $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RegisterService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');

        if (!$user = User::where('email', $email)->first()) {
            $this->error('Undefined user with email ' . $email);
            return false;
        }

        try {
            $this->service->verify($user->confirmation_token);            
        } catch (\DomainException $e) {
            $this->error($e->getMessage());
            return false;
        }
        
        $this->info('Mail is successfully confirmed');
        return true;
    }
}
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTaskRequset extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            'title' => 'required|max:100',
            'description' => 'required|max:255',            
        ];
    }

    public function taskFillData()
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'status_id' => $this->status_id,            
        ];
    }
}

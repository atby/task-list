<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Requests\StoreTaskRequset;
use App\Http\Controllers\Controller;
use App\Task;
use App\Status;
use App\Comment;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Task $task, Status $status)
    {
        $tasks = $task->latest()->with(['status', 'comments'])->withCount('comments')->get();
       
        return view('dashboard.tasks.index', [
            'title' => 'Задачи',
            'statuses' => $status->all(),
            'tasks' => $tasks,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Status $status)
    {
        return view('dashboard.tasks.create',[
            'title' => 'Добавление задачи',
            'statuses' => $status->all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequset $request, Task $task)
    {
        $task = $task->fill($request->taskFillData());
        $task->save();

        return redirect()->route('tasks.index')->with('status', __('messages.task.store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task, Status $status)
    {
        return view('dashboard.tasks.edit', [
            'title' => 'Редактирование задачи',
            'task' => $task,
            'statuses' => $status->all(),
            'taskStatus' => $task->status,
            'comments' => $task->comments,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTaskRequset $request, Task $task)
    {
        $task = $task->fill($request->taskFillData());
        $task->save();

        return redirect()->route('tasks.index')->with('status', __('messages.task.update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        
        return back()->with('status', __('messages.task.delete'));
    }
}

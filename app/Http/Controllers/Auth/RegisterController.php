<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Services\Auth\RegisterService;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{

    use RegistersUsers;

    private $service;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RegisterService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    public function register(RegisterRequest $request)
    {
        $this->service->register($request);
        return redirect()->route('login')
            ->with('success', 'Чтобы подтвердить адрес электронной почты, нажмите на ссылку в полученном письме.');
    }

    public function verify()
    {
        if (!$this->service->verify(request('token'))) {
            return redirect(route('login'))->with('error', 'Unknown token.');
        }

        return redirect(route('login'))
            ->with('success', 'Ваша учетная запись теперь подтверждена!');
    }
}

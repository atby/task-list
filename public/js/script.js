$(document).ready(function() {
  $(document).on('click', '.edit-modal', function() {
        $('#footer_action_button').text(" Редактировать");
        $('#footer_action_button').removeClass('fas fa-trash-alt');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Редактировать');
        $('.deleteContent').hide();
        $('.form-horizontal').show();
        $('#fid').val($(this).data('id'));
        $('#n').val($(this).data('name'));
        $('#myModal').modal('show');
    });
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Удалить");
        $('#footer_action_button').addClass('fas fa-trash-alt');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Удалить');
        $('#did').val($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.dname').html($(this).data('name'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.edit', function() {

        $.ajax({
            type: 'put',
            url: '/dashboard/comments/' + $("#fid").val(),
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#fid").val(),
                'content': $('#n').val()
            },
            success: function(data) {
                $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td></td><td>" + data.content + "</td></td><td>" + data.created_at + "</td><td>" + data.updated_at + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.content + "'><span class='fas fa-edit'></span></button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.content + "'><span class='fas fa-trash-alt'></span></button></td></tr>");
            }
        });
    });
    $("#add").click(function() {

        $.ajax({
            type: 'post',
            url: '/dashboard/comments',
            data: {
                '_token': $('input[name=_token]').val(),
                'content': $('input[name=content]').val(),
                'task_id': $('input[name=task_id]').val(),

            },
            success: function(data) {
                if ((data.errors)){
                  $('.error').removeClass('invisible');
                    $('.error').text(data.errors.name);
                }
                else {
                    $('.error').addClass('invisible');
                    $('#table').append("<tr class='item" + data.id + "'><td></td><td>" + data.content + "</td></td><td>" + data.created_at + "</td><td>" + data.updated_at + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.content + "'><span class='fas fa-edit'></span></button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.content + "'><span class='fas fa-trash-alt'></span></button></td></tr>");
                }
            },

        });
        $('#name').val('');
    });
    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'delete',
            url: '/dashboard/comments/' + $("#did").val(),
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#did").val()
            },
            success: function(data) {
                $('.item' + $("#did").val()).remove();
            }
        });
    });
});

$('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                this.closest('form').submit();
            }
}); 